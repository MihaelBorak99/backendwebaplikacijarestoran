import axios from "axios"

class KorisnikDataService{

    dodajNovogKorisnika(name,korisnik){
        return axios.post(`http://localhost:9192/korisnici/${name}/dodajKorisnika/`, korisnik)
    }
}

export default new KorisnikDataService()