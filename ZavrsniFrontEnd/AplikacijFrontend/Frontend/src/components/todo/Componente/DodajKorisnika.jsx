import { Checkbox } from '@material-ui/core'
import React,{Component, useState} from 'react'
import AuthenticationService from '../Api/Service/AuthenticationService'
import KorisnikDataService from '../Api/Service/KorisnikDataService'



class DodajKorniskaComponent extends Component{

    constructor(props){
        super(props)
        this.state = {
            username: '',
            password: '',
            ponovniPassword: '',
            hasLoginFailed: false,
            showSuccessMessage: false,
            admin: false,
            poruka: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.dodajKorisnika = this.dodajKorisnika.bind(this)
    }

    dodajKorisnika(){
        let username = AuthenticationService.getLoggedInUsername()
        let korisnik = {username: this.state.username, password: this.state.password, admin: this.state.admin, password2: this.state.ponovniPassword}
        KorisnikDataService.dodajNovogKorisnika(username,korisnik)
        .then(response => {
            console.log({poruka : response.data})
            this.setState(() =>{
                return { poruka:  response.data}
               }
            )
        })

    }

render(){
    
         return(
            <div>

            <div className='dodajKorisnika'>
                
               <div><label className='leftSide'>Unesite korisničko ime: </label><input type="text" name= "username"  value={this.state.username} onChange={this.handleChange}></input></div>
               <div> <label className='leftSide'>Unesite lozinku: </label><input type="password" name= "password" value={this.state.password} onChange={this.handleChange}></input></div> 
               <div><label className='leftSide'>Ponovno unesite lozinku: </label><input type="password" name= "ponovniPassword" value={this.state.ponovniPassword} onChange={this.handleChange}></input></div>
               <div className='desnoo'>
                <input  type="checkbox" value="admin" name="Admin" onClick={()=> this.setState({admin : true})}/> Admin 
                </div>
            </div>
            <div>
                <button className='btn btn-info btn-dodajKorisnika' onClick={()=> this.dodajKorisnika()}> Dodaj novog korisnika</button>
            </div>
            </div>
            )
}

handleChange(event){
    console.log(event.target.name)
    this.setState({ [event.target.name]: event.target.value})
}
}

export default DodajKorniskaComponent