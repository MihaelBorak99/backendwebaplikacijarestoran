import React, { Component} from 'react'


class WelcomeComponent extends Component{

    constructor(props){
        super(props)
        this.state = {
            welcomeMessage : '',
        }
    }
    render(){
        return(
                <>
                    <h1>Dobrodošli korisniče: {this.props.params.name}</h1>
                    <div className='container'>     
                    <h3>Poslužite se navigacijskom trakom na vrhu kako bi pristupili funkcionalnostima aplikacije</h3>
                    </div>
                </>
        )

    }

}

export default WelcomeComponent