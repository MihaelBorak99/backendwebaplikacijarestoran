import React, {Component} from 'react'
import moment from 'moment'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import ArticleDataService from '../Api/Service/ArticleDataService'
import AuthenticationService from '../Api/Service/AuthenticationService'

class ArticleComponent extends Component{

    constructor(props){
        super(props)

        this.state = {
            id: this.props.params.id,
            naziv :'',
            kategorija :'',
            cijena: ''
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.validate = this.validate.bind(this)
}

componentDidMount(){
    if(this.state.id === -1){
        return
    }
    let username = AuthenticationService.getLoggedInUsername()
    ArticleDataService.dohvatiArtikal(username,this.state.id)
    .then(response => this.setState(
        {
            naziv: response.data.naziv,
            kategorija: response.data.kategorija,
            cijena: response.data.cijena
        }
    ) )
}
validate(values){
    let errors={}
    if(!values.description){
        errors.description = 'Enter a Description'
    } else if(values.description.length <5){
        errors.description = 'Enter atleast 5 Characters in Description'
    }
    if(!moment(values.targetDate).isValid()){
        errors.targetDate = 'Enter a valid Target Date'
    }

    return errors
}
onSubmit(values){
    let username = AuthenticationService.getLoggedInUsername()
    let artikal ={id: this.state.id, naziv: values.naziv,kategorija: values.kategorija,cijena: values.cijena}
   // if(this.state.id === -1){
      //  ArticleDataService.dodajNoviArtikal(username,artikal).then (()=> this.props.navigate('/artikli'))
    
        ArticleDataService.promijeniArtikal(username,this.state.id,artikal).then (()=> this.props.navigate('/artikli'))
    }


    render(){
        let {naziv,kategorija,cijena} = this.state
        return(
         <div>
            <h1>Artikal</h1>
             <div className = "container"> 
                 <Formik 
                    initialValues={{naziv,kategorija,cijena}}
                    onSubmit={this.onSubmit}
                    validateOnChange={false}
                    validateOnBlur={false}
                    enableReinitialize={true}
                    >
                    {
                        (props) => (
                            <Form>
                                <ErrorMessage name="description" component="div" 
                                className ="alert alert-warning"></ErrorMessage>
                                <ErrorMessage name="targetDate" component="div" 
                                className ="alert alert-warning"></ErrorMessage>
                                <fieldset className="form-group">
                                    <label>Naziv</label>
                                    <Field className="form-control" type = "text" name="naziv"></Field>
                                </fieldset>
                                <fieldset className="form-group">
                                    <label>Kategorija</label>
                                    <Field className="form-control" type = "text" name="kategorija"></Field>
                                </fieldset>
                                <fieldset className="form-group">
                                    <label>Cijena</label>
                                    <Field className="form-control" type ="number" name="cijena"></Field>
                                </fieldset>
                                <button className="btn btn-success" type="submit">Save</button>
                            </Form>
                        )
                    }
                </Formik>
            </div>    
         </div>
        )
    }
}


export default  ArticleComponent